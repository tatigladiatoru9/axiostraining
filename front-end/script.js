document.querySelector("#submit").addEventListener("click", () => {
  toastr.remove();
  const user = {
    nume: document.querySelector("#name").value,
    prenume: document.querySelector("#prenume").value,
    email: document.querySelector("#email").value
  };

  let valid = true;
  if (!user.nume || !user.prenume || !user.email) {
    valid = false;
    toastr.error("Fielduri necompletate!");
  }

  if (valid == true) {
    axios
      .post("/api/addUser", {
        nume: user.nume,
        prenume: user.prenume,
        email: user.email
      })
      .then(response => {
        toastr.success("User added!");
      })

      .catch(error => {
        const values = Object.values(error.response.data);
        values.map(item => {
          toastr.error(item);
        });
      });
  }
});

const displayUsers = response => {
  response.data.forEach(el => () => {
    const markup = `
        <p>Name: ${el.nume}</p>
        <p>Name: ${el.prenume}</p>
        <p>Name: ${el.email}</p>
        `;
    document
      .querySelector(".form__output")
      .insertAdjacentHTML("afterbegin", markup);
  });
};

const getUser = async () => {
  const response = await axios.get("/api/getUsers");
  displayUsers(response);
  console.log(response);
};

document.querySelector("#get").addEventListener("click", () => {
  getUser();
});

document.querySelector("#update").addEventListener("click", () => {
  toastr.remove();
  const user = {
    id: document.querySelector("#id").value,
    nume: document.querySelector("#name2").value,
    prenume: document.querySelector("#prenume2").value,
    email: document.querySelector("#email2").value
  };

  let valid = true;
  if (!user.nume || !user.prenume || !user.email) {
    valid = false;
    toastr.error("Fielduri necompletate!");
  }

  if (valid == true) {
    axios
      .put("/api/updateUser", {
        id: user.id,
        nume: user.nume,
        prenume: user.prenume,
        email: user.email
      })
      .then(response => {
        toastr.success("User updated!");
      })

      .catch(error => {
        const values = Object.values(error.response.data);
        values.map(item => {
          toastr.error(item);
        });
      });
  }
});

document.querySelector("#delete").addEventListener("click", () => {
  const user = { id: document.querySelector("#id2").value };
  axios.post("/api/deleteUser", {
    id: user.id
  });
  toastr.success("User with id: " + user.id + " deleted!");
});
